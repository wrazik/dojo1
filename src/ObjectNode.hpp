#pragma once
#include "Node.hpp"

class ObjectNode : public Node
{
public:
    typedef Childs value_type;
    static constexpr Type type = Type::object;

    explicit ObjectNode(TagName const& tagName);
    virtual ~ObjectNode() = default;

public:
    operator std::string() const override;
    Childs& childs() override;
    Childs const& childs() const override;
    TagName const& tagName() const override;
    Attributes& attributes() override;
    Attributes const& attributes() const override;
    std::ostream& serialize(std::ostream& stream) const override;

private:
    Childs m_childs;
    Attributes m_attributes;
    TagName m_tag;
};
