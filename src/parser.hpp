#pragma once

#include "Node.hpp"
#include "IHandler.hpp"
#include <stack>

namespace parser
{
using NodePtr = std::unique_ptr<Node>;
NodePtr parse(const std::string&);
NodePtr parse(IHandler& handler, const std::string&);
}
