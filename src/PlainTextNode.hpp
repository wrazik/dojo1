#pragma once
#include "Node.hpp"

class PlainTextNode : public Node
{
public:
    static constexpr Type type = Type::plainText;
    typedef std::string value_type;
    explicit PlainTextNode(value_type const& content);
    ~PlainTextNode() = default;

public:
    operator std::string() const override;
    Childs& childs() override;
    Childs const& childs() const override;
    TagName const& tagName() const override;
    Attributes& attributes() override;
    Attributes const& attributes() const override;
    std::ostream& serialize(std::ostream& stream) const override;

private:
    value_type m_content;
};
