#pragma once
#include <exception>
#include <list>
#include <map>
#include <memory>
#include <ostream>
#include <stdexcept>
#include <string>

class Node
{
public:
    typedef std::string TagName;
    typedef std::map<std::string, std::string> Attributes;
    typedef std::list<std::unique_ptr<Node>> Childs;
    enum class Type
    {
        plainText,
        object
    };

    Type const& getType() const
    {
        return m_type;
    }
    virtual operator std::string() const = 0;
    virtual Childs& childs() = 0;
    virtual Childs const& childs() const = 0;
    virtual TagName const& tagName() const = 0;
    virtual Attributes& attributes() = 0;
    virtual Attributes const& attributes() const = 0;
    virtual std::ostream& serialize(std::ostream& stream) const = 0;
    virtual ~Node() = default;

protected:
    explicit Node(Type const& type);

private:
    const Type m_type;
};
