#include "EventHandler.hpp"

EventHandler::EventHandler(Node& /*node*/, EventHandler::HandlerStack& /*stack*/)
    : IHandler()
//    , node_(node)
//    , stack_(stack)
{
}

void EventHandler::handleTaggedTextToken(const std::string& taggedToken)
{
    if (taggedToken[1] == '/')
    {
        handleTagEndToken(taggedToken);
    }
    else if (taggedToken[taggedToken.size() - 2] == '/')
    {
        handleSelfClosingToken(taggedToken);
    }
    else
    {
        handleTagBeginToken(taggedToken);
    }
}

void EventHandler::handleSelfClosingToken(const std::string& /*taggedToken*/)
{
}

void EventHandler::handleTagBeginToken(const std::string& /*taggedToken*/)
{
}

void EventHandler::handleTagEndToken(const std::string& /*taggedToken*/)
{
    //HandlerPtr me;
    //me.swap(stack_.top());

    //    if(node_.tagName() ~== taggedToken)
    //    {
    //        stack_.pop();
    //    }
    //    else
    //    {
    //        throw std::runtime_error(node_.tagName() + "not match to: " + taggedToken);
    //    }
}
