#pragma once

#include "IHandler.hpp"
#include "Node.hpp"
#include <stack>

class EventHandler : public IHandler
{
public:
    typedef std::stack<HandlerPtr> HandlerStack;

    EventHandler(Node&, HandlerStack& stack);
    void handleTaggedTextToken(const std::string& taggedToken) override;
    void handleSelfClosingToken(const std::string& taggedToken) override;
    void handleTagBeginToken(const std::string& taggedToken) override;
    void handleTagEndToken(const std::string& taggedToken) override;

private:
    //Node& node_;
    //HandlerStack& stack_;
};
