#include <algorithm>

#include "parser.hpp"
#include "PlainTextNode.hpp"

namespace parser
{

NodePtr parse(const std::string&)
{
    // here create std::stack<IHandler> handler
    // and call parse(handler, text);
    return nullptr;
}

NodePtr parse(IHandler& handler, const std::string& text)
{
    if (text.empty())
    {
        return nullptr;
    }

    auto it = text.cbegin();

    while (it != text.end())
    {

        if (*it == '<')
        {
            auto end = std::find(it + 1, text.end(), '>');
            if (end != text.end())
            {
                handler.handleTaggedTextToken({ it, end + 1 });
                //event
                it = end + 1;
            }
            else
            {
                break;
            }
        }
        else
        {
            auto end = std::find(it + 1, text.end(), '<');
            if (end != text.end())
            {
                handler.handlePlainTextToken({ it, end });
                //event
                it = end;
            }
            else
            {
                handler.handlePlainTextToken({ it, end });
                break;
            }
            //it++;
        }
    }

    return nullptr;
}
}
