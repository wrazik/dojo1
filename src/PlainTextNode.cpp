#include "PlainTextNode.hpp"

PlainTextNode::PlainTextNode(const PlainTextNode::value_type& content)
    : Node(Type::plainText)
    , m_content(content)
{
}

PlainTextNode::operator std::string() const
{
    return m_content;
}

Node::Childs& PlainTextNode::childs()
{
    throw std::runtime_error("PlainTextNode don't have childs by definition");
}

const Node::Childs& PlainTextNode::childs() const
{
    throw std::runtime_error("PlainTextNode don't have childs by definition");
}

const Node::TagName& PlainTextNode::tagName() const
{
    throw std::runtime_error("PlainTextNode don't have tag name by definition");
}

Node::Attributes& PlainTextNode::attributes()
{
    throw std::runtime_error("PlainTextNode don't have attributes by definition");
}

const Node::Attributes& PlainTextNode::attributes() const
{
    throw std::runtime_error("PlainTextNode don't have attributes by definition");
}

std::ostream& PlainTextNode::serialize(std::ostream& stream) const
{
    stream << m_content;
    return stream;
}
