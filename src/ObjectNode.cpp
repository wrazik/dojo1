#include "ObjectNode.hpp"
#include <sstream>

ObjectNode::ObjectNode(const Node::TagName& tagName)
    : Node(Type::object)
    , m_childs()
    , m_attributes()
    , m_tag(tagName)
{
}

ObjectNode::operator std::string() const
{
    std::stringstream stream;
    serialize(stream);
    return stream.str();
}

Node::Childs& ObjectNode::childs()
{
    return m_childs;
}

const Node::Childs& ObjectNode::childs() const
{
    return m_childs;
}

const Node::TagName& ObjectNode::tagName() const
{
    return m_tag;
}

Node::Attributes& ObjectNode::attributes()
{
    return m_attributes;
}

const Node::Attributes& ObjectNode::attributes() const
{
    return m_attributes;
}

std::ostream& ObjectNode::serialize(std::ostream& stream) const
{
    stream << "<" << m_tag << "";
    for (auto const& attr : m_attributes)
    {
        stream << " " << attr.first << "="
               << "\"" << attr.second << "\"";
    }

    if (m_childs.empty())
    {
        stream << "/>";
    }
    else
    {
        stream << ">";
        for (auto const& child : m_childs)
        {
            child->serialize(stream);
        }
        stream << "</" << m_tag << ">";
    }
    return stream;
}
