#pragma once
#include <string>
#include <memory>
class IHandler
{
public:
    typedef std::unique_ptr<IHandler> HandlerPtr;
    virtual void handlePlainTextToken(const std::string& token) = 0;
    virtual void handleTaggedTextToken(const std::string& taggedToken) = 0;
    virtual void handleTagBeginToken(const std::string& token) = 0;
    virtual void handleTagEndToken(const std::string& token) = 0;
    virtual void handleSelfClosingToken(const std::string& token) = 0;
    virtual ~IHandler() = default;
};
