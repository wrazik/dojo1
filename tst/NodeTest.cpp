#include <gtest/gtest.h>
#include <memory>
#include <Node.hpp>
#include <ObjectNode.hpp>
#include <PlainTextNode.hpp>
#include <sstream>

// there is something common between tests?
class PlainText : public ::testing::Test
{
public:
    PlainText()
        : ::testing::Test()
        , text("some text")
        , node(new PlainTextNode(text))
    {
    }

public:
    const std::string text;
    std::unique_ptr<Node> node;
};

class Object : public ::testing::Test
{
public:
    Object()
        : ::testing::Test()
        , text("example")
        , node(new ObjectNode(text))
    {
    }

public:
    const std::string text;
    std::unique_ptr<Node> node;
};

TEST_F(PlainText, Throw_WhenTagNameCalled)
{
    EXPECT_THROW(node->tagName(), std::runtime_error);
}

TEST_F(PlainText, Throw_WhenAttributesCalled)
{
    EXPECT_THROW(node->attributes(), std::runtime_error);
}

TEST_F(PlainText, PlainTextTypeReturned_WhenTypeAskedOnBase)
{
    EXPECT_EQ(node->getType(), Node::Type::plainText);
}

TEST_F(PlainText, TheSameContentReturnedAsGivenInCtor_WhenStringConversionOperatorCalled)
{
    EXPECT_EQ(std::string(*node), text);
}

TEST_F(PlainText, IsSerializable)
{
    std::stringstream stream;

    node->serialize(stream);

    EXPECT_EQ(stream.str(), text);
}

TEST_F(Object, ObjectTypeReturned_WhenTypeAskedOnBase)
{
    EXPECT_EQ(Node::Type::object, node->getType());
}

TEST_F(Object, TheSameTagNameReturnedAsGivenInCtor)
{
    EXPECT_EQ("example", node->tagName());
}

TEST_F(Object, NoChildsNoAttrsSerialization)
{
    std::stringstream stream;
    node->serialize(stream);
    EXPECT_EQ("<example/>", stream.str());
}

TEST_F(Object, NoChildsButWithAttrsSerialization)
{
    auto& attributes(node->attributes());
    attributes.insert({ "attr2", "value1" });
    attributes.insert({ "attr1", "value2" });
    attributes.insert({ "attr4", "value3" });
    attributes.insert({ "attr3", "value4" });
    std::stringstream stream;
    node->serialize(stream);
    EXPECT_EQ("<example attr1=\"value2\" attr2=\"value1\" attr3=\"value4\" attr4=\"value3\"/>", stream.str());
}

TEST_F(Object, WithChildsAndAttrsSerialization)
{
    node->attributes().insert({ "attr2", "value1" });

    node->childs().emplace_back(std::unique_ptr<Node>(new ObjectNode("child")));
    node->childs().front()->childs().emplace_back(std::unique_ptr<Node>(new PlainTextNode("some text")));
    node->childs().front()->attributes().insert({ "attr1", "value2" });
    std::stringstream stream;
    node->serialize(stream);
    EXPECT_EQ("<example attr2=\"value1\"><child attr1=\"value2\">some text</child></example>", stream.str());
}
