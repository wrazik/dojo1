#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <IHandler.hpp>
#include <parser.hpp>

class MockHandler : public IHandler
{
public:
    MockHandler()
        : IHandler()
    {
    }
    MOCK_METHOD1(handlePlainTextToken, void(const std::string& token));
    MOCK_METHOD1(handleTaggedTextToken, void(const std::string& token));
    MOCK_METHOD1(handleTagBeginToken, void(const std::string& token));
    MOCK_METHOD1(handleTagEndToken, void(const std::string& token));
    MOCK_METHOD1(handleSelfClosingToken, void(const std::string& token));
    virtual ~MockHandler() = default;
};

TEST(DUPA, TestIfPlainTestIsFound)
{
    MockHandler handler;
    EXPECT_CALL(handler, handlePlainTextToken("TEXT"));

    parser::parse(handler, "TEXT");
}

TEST(DUPA, TestIfPlainTestwithTagIsFound)
{
    MockHandler handler;
    EXPECT_CALL(handler, handlePlainTextToken("TEXT"));
    EXPECT_CALL(handler, handleTaggedTextToken("<dupas>"));
    EXPECT_CALL(handler, handlePlainTextToken("niedupas"));
    EXPECT_CALL(handler, handleTaggedTextToken("</dupas>"));

    parser::parse(handler, "TEXT<dupas>niedupas</dupas>");
}
