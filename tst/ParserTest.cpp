#include <gtest/gtest.h>
#include <parser.hpp>

// there is something common between tests?
TEST(Parser, ReturnNullWhenEmpty)
{
    auto parsed = parser::parse("");
    EXPECT_EQ(nullptr, parsed);
}

TEST(Parser, DISABLED_ReturnParsedTag)
{
    const std::string text("Text");

    auto node = parser::parse(text);

    ASSERT_TRUE(node);
    ASSERT_EQ(node->getType(), Node::Type::plainText);
    EXPECT_EQ(std::string(*node), text);
}

TEST(Parser, DISABLED_ReturnEmptyObjectNode1)
{
    const std::string text("<emptyTag/>");

    auto node = parser::parse(text);

    ASSERT_TRUE(node);
    ASSERT_EQ(node->getType(), Node::Type::object);
    EXPECT_EQ(std::string(*node), text);
}

TEST(Parser, DISABLED_ReturnEmptyObjectNode2)
{
    const std::string text("<emptyTag></emptyTag>");

    auto node = parser::parse(text);

    ASSERT_TRUE(node);
    ASSERT_EQ(node->getType(), Node::Type::object);
    EXPECT_EQ(std::string(*node), text);
}

TEST(Parser, DISABLED_ReturnEmptyObjectNode3)
{
    const std::string text("<emptyTag><emptyTag></emptyTag></emptyTag>");

    auto node = parser::parse(text);

    ASSERT_TRUE(node);
    ASSERT_EQ(node->getType(), Node::Type::object);
    EXPECT_EQ(std::string(*node), text);
}

TEST(Parser, DISABLED_Scenario1)
{
    const std::string text("<emptyTag>text<emptyTag>text</emptyTag>text</emptyTag>");

    auto node = parser::parse(text);

    ASSERT_TRUE(node);
    ASSERT_EQ(node->getType(), Node::Type::object);
    EXPECT_EQ(std::string(*node), text);
}

TEST(Parser, DISABLED_Scenario2)
{
    const std::string text("<example attr2=\"value1\"><child attr1=\"value2\">some text</child></example>");

    auto node = parser::parse(text);

    ASSERT_TRUE(node);
    ASSERT_EQ(node->getType(), Node::Type::object);
    EXPECT_EQ(std::string(*node), text);
}
